\documentclass[a4paper,12pt]{article}
%Automatos Finitos, Linguagens Regulares e Expressões Regulares

\usepackage[utf8x]{inputenc}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{arrows,automata}

%opening
\title{Teoria da Computação}
\author{Inácio Alves}
\newtheorem{definicao}{Definição}[section]
\newtheorem{teo}{Teorema}[section]
\newtheorem{ex}{Exemplo}[section]

\begin{document}

\maketitle

\section{Linguagens}
\subsection{Alfabetos, Cadeias e Linguagens}

  Um {\bf alfabeto} é um conjunto finito e não-vazio. Os elementos de um
alfabeto são chamados de {\bf símbolos} ou {\bf letras} do alfabeto. Usaremos
letras gregas maiúsculas como $\Sigma$ e $\Gamma$ para designar alfabetos e
letras de \verb!tamanho fixo! para denotar os símbolos de um alfabeto.

\begin{enumerate}
  \item $\Sigma_1=\{\mathtt{0,1}\}$
  \item
$\Sigma_1=\{\mathtt{a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,w,x,y,z}\}$
  \item $\Gamma=\{\mathtt{0,1,x,y,z}\}$
\end{enumerate}
São exemplos de alfabetos.

Uma {\bf cadeia sobre um alfabeto} é uma sequência finita de símbolos de tal
alfabeto, geralmente escritos um após o outro sem qualquer separador.

Se $\Sigma_1=\{\mathtt{0,1}\}$ então \verb!010110! é uma cadeia sobre
$\Sigma_1$. Do mesmo modo, se $\Sigma_2=\{\mathtt{a,b,c,...,z}\}$ então
\verb!ifcemaracanau! é uma cadeia sobre $\Sigma_2$.

Se $w$ é uma cadeia sobre $\Sigma$ então o {\bf comprimento} de $w$, denotado
por $|w|$ é o numero de símbolos (contando as repetições) que $w$ contém. A
cadeia de comprimento zero é a {\bf cadeia vazia} denotada por $\varepsilon$.

Se $w$ tem comprimento $n$, podemos escrever $w=w_1w_2\cdots w_n$ onde cada
$w_i \in \Sigma$. O {\bf reverso} de $w$, denotado por $w^R$, é definido,
indutivamente, pondo:
\begin{enumerate}
  \item $\varepsilon^R=\varepsilon$
  \item Se $w$ tem comprimento $n+1$ então $w=w_1w_2\cdots w_na$ e
$w^R=a(w_1w_2\cdots w_n)^R$
\end{enumerate}

Dadas duas cadeias $w$ e $v$ sobre $\Sigma$, a {\bf concatenação} de $w$ e $v$,
denotada por $w\circ v$ ou simplesmente $wv$, é uma nova cadeia que consiste na
justaposição dos símbolos de $w$ com os símbolos de $v$. Se denotarmos $w$ como
uma função $w:\{0,1,2,\ldots,|w|\}\rightarrow\Sigma$ tal que $w(j)$ é o
$j$-ésimo símbolo na cadeia $w$, então $u=wv$ é tal que $u(j)=w(j)$ para todo
$j=0,1,2,\ldots,|w|$ e $u(|w|+j)=v(j)$ para todo
$j=0,1,2,\ldots,|v|$\footnote{faça uma implementação equivalente a função
{\tt strcat} de C}.

É claro que $w\varepsilon=\varepsilon w=w$ qualquer que seja a cadeia $w$.

Se $w=uvz$ sendo $u$, $v$ e $z$ cadeias, dizemos que $v$ é uma subcadeia de
$w$. Note que $w=\varepsilon\varepsilon w$ de onde segue que $\varepsilon$ é
uma subcadeia de qualquer cadeia.

Se $w=uv$ para duas cadeias quaisquer $u$ e $v$, dizemos que $u$ é um
{\bf prefixo} de $w$ e que $v$ é um {\bf sufixo} de $w$.

Se $w$ e uma cadeia qualquer, defina $w^n$ pondo:
\begin{enumerate}
  \item $w^0=\varepsilon$
  \item $w^{n+1}=w^nw$
\end{enumerate}

O conjunto de todas as cadeias sobre $\Sigma$ é denotado por $\Sigma^*$. Uma
{\bf linguagem} $L$ é qualquer subconjunto de $\Sigma^*$. Logo $\emptyset$,
$\Sigma$ e $\Sigma^*$ são linguagens.

Se $L_1$ e $L_2$ são linguagens sobre $\Sigma_1$ e $\Sigma_2$, respectivamente,
podemos definir uma nova linguagem $L$ que é a {\bf concatenação das
linguagens} $L_1$ e $L_2$, denotada por $L=L_1\circ L_2$, ou simplesmente
$L=L_1L_2$, pondo
$$L=\{w;w=xy\textrm{ onde }x\in L_1\textrm{ e }y\in L_2\}$$
Note que $L$ é uma linguagem sobre $\Sigma=\Sigma_1\cup\Sigma_2$ e que tanto
$L_1$ quanto $L_2$ são subconjuntos de $L$.

Seja $L$ uma linguagem sobre $\Sigma$, definimos a {\bf estrela de Kleene} de
$L$, denotada por $L^*$ pondo
$$L^*=\{w\in\Sigma^*;w=w_0w_1w_2\cdots w_k\textrm{ para algum
}k\geq0\textrm{ e }w_i\in L\}$$
isto é, $L^*$ consiste na concatenação de $0$ ou mais cadeias de $L$.
Usamos $L^+$ para denotar a linguagem $LL^*$.

\begin{enumerate}
  \item Mostre que $\Sigma^*$ é a estrela de Kleene da linguagem $\Sigma$
  \item Mostre que se $L_1$ e $L_2$ são linguagens tais que $L_1\subseteq L_2$
        então $L_1^*\subseteq L_2^*$
  \item Mostre que $L^+$ é a menor linguagem que contém $L$ e todas as
        concatenações de cadeias de $L$.
\end{enumerate}

Por exemplo, sejam o alfabeto padrão com 26
letras, $\Sigma=\{\mathtt{a,b,c,...,z}\}$, $A=\{\mathtt{ifce,ufc}\}$ e
$B=\{\mathtt{fortaleza,maracanau}\}$ então
\begin{itemize}
  \item $A\cup B=\{\mathtt{ifce,ufc,maracanau,fortaleza}\}$
  \item $A\circ
B=\{\mathtt{ifcemaracanau,ifcefortaleza,ufcmaracanau,ufcfortaleza}\}$
  \item
$A^*=\{\varepsilon,\mathtt{ifce,ufc,ifceifce,ifceufc,ufcufc,ifceifceifce,
ifceifceufc, ...}\} $
\end{itemize}
\subsection{Expressões Regulares}
Dizemos que $R$ é uma {\bf expressão regular} quando:
\begin{enumerate}
  \item $R=\emptyset$
  \item $R=\varepsilon$
  \item $R=a$ para algum $a\in\Sigma$
  \item $(R_1\cup R_2)$ onde $R_1$ e $R_2$ são expressões regulares.
  \item $(R_1\circ R_2)$ onde $R_1$ e $R_2$ são expressões regulares.
  \item $(R_i^*)$ onde $R_i$ é expressão regular.
\end{enumerate}
No item 1, $R$ é a linguagem vazia, isto é, a linguagem que não possui palavras.

Em 2, $R$ é a linguagem $\{\varepsilon\}$, isto é, a linguagem que contém
apenas a palavra vazia.

Em 3, $R=\{a\}$. Nos itens 4 e 5, $R$ é, respectivamente, a união e a
concatenação de $R_1$ e $R_2$. Enquanto no item 6 $R$ é a estrela de $R_i$.

Do mesmo modo que escrevemos $L^+$, denotamos $RR^*$ por $R^+$. Isto é,
Enquanto $R^*$ contém zero ou mais concatenações de $R$, $R^+$ possui uma ou
mais concatenações de $R$. Assim sendo, $R^+\cup\varepsilon=R^*$.

\section{Automatos Finitos}
Considere uma porta com um sensor de presença que funciona do seguinte modo.
\begin{enumerate}
  \item O sensor consiste de dois tapetes, um na parte frontal e outro na parte
        de trás da porta.
  \item O controlador pode estar no estado {\tt ABERTO} ou no estado {\tt
        FECHADO} indicando a situação da porta.
  \item Existem quatro possibilidades quanto a presença de pessoas, a saber:
        \begin{enumerate}
          \item {\tt FRENTE} indicando que uma pessoa está sobre o tapete
                frontal.
          \item {\tt ATRÁS} indicando que uma pessoa está sobre o tapete
                na parte de trás da porta.
          \item {\tt AMBOS} indicando que existe uma pessoa sobre cada tapete.
          \item {\tt NENHUM} indicando que não há qualquer pessoa sobre os
                tapetes.
        \end{enumerate}
\end{enumerate}

\begin{center}
  \begin{tikzpicture}[node distance = 2cm, auto]
  \tikzstyle{block} = [rectangle, draw, fill=black!10, 
    text width=5em, text centered, rounded corners, minimum
    height=4em]
    
    \node[block] (tf) {tapete\\frontal};
    \node[right of=tf] (m) {};
    \node[above of=m] (a) {};
    \node[below of=m] (b) {porta};
    \draw (2,-1.3) coordinate (a_1) -- (2,1.3) coordinate (a_2);
    \node[block, right of=m] (tt) {tapete\\posterior};
    \path (a)--(b);
  \end{tikzpicture}
\end{center}

A porta é uma comum, destas que temos na entrada de casa ou de uma sala de
aula, isto é, ela abre girando para dentro do ambiente. Isto significa que a
porta não pode se abrir caso tenha alguém sobre o tapete atrás da porta.

Podemos representar todas os estados da porta bem como as mudanças através de
um {\bf diagrama de estados} como o abaixo:
  
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  \tikzstyle{every state}=[fill=black!10,draw=none,text=black]
    
  \node[initial,state] (A) {fechada};
  \node[state] (B) [right of=A,node distance=6cm] {aberta};

  \path (B) edge [loop above] node {atrás,ambos,frente} (B)
        (A) edge [loop above] node {atrás,ambos,nenhum} (A);
  \path  [-latex] (A.south east) edge [bend right]  (B.south west) ;
  \path  [-latex] (B.north west) edge [bend right] (A.north east);
  \node at (2.8,1.5)  (af) {nenhum};
  \node at (2.8,-1.5)  (fa) {frente};
\end{tikzpicture}

Tal diagrama pode ser representado em uma tabela como
\begin{center}
  \begin{tabular}{ll|llll}
   & & \multicolumn{4}{c}{sinal de entrada}\\
   & & {\tt NENHUM} & {\tt FRENTE} & {\tt ATRÁS} & {\tt AMBOS} \\
   \cline{2-6}
estado & {\tt FECHADO} & {\tt FECHADO} & {\tt ABERTO} & {\tt FECHADO} &
{\tt FECHADO}\\  
          & {\tt ABERTO}  & {\tt FECHADO} & {\tt ABERTO} & {\tt ABERTO} & {\tt
ABERTO}\\
  \end{tabular}
\end{center}

Vamos começar um estudo mais formal da teoria de autômatos finitos. Considere a
figura abaixo representando um autômatos finito $M_1$.
\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  \tikzstyle{every state}=[fill=black!10,draw=black,text=black]
    
  \node[initial,state] (q1) {$q_1$};
  \node[accepting,state] (q2) [right of=q1] {$q_2$};
  \node[state] (q3) [right of=q2,node distance=4cm] {$q_3$};

  \path (q1) edge node {1} (q2)
        (q1) edge [loop above] node {0} (q1)
        (q2) edge [loop above] node {1} (q2)
        (q2.north east) [bend left=20]  edge  node {0} (q3.north west)
        (q3.south west) edge [below right] node {0,1} (q2.south east);
\end{tikzpicture}
\end{center}
A figura acima é denominada de {\bf diagrama de estado} de $M_1$. O autômato
tem três estados $q_1$, $q_2$ e $q_3$. O {\bf estado inicial} é $q_1$, o que é
indicado pela seta apontando para ele com a expressão {\tt start}. O {\bf
estado de aceitação} é $q_2$, aquele que possui um círculo duplo. As setas de
um estado para outro são chamadas de {\bf transições}. Os rótulos sobre tais
setas indicam qual símbolo do alfabeto é responsável pela transição.

Quando recebe uma cadeia, tal como {\tt 1101},  a saída de $M_1$ será {\bf
aceita} ou {\bf rejeita} (por hora, consideremos sim/não). O processamento
começa no estado inicial de $M_1$. O autômato recebe cada símbolo e move-se de
um estado para outro ao longo da transição que tem aquele símbolo como rótulo.
A saída será {\it aceita} se $M_1$ estiver em um estado de aceitação e {\it
rejeita} caso contrário.

Simulando as transições de $M_1$ para a cadeia {\tt 1101} obtemos:
\begin{enumerate}
  \item Começa no estado $q_1$
  \item Lê {\tt 1}, segue a transição de $q_1$ para $q_2$
  \item Lê {\tt 1}, segue a transição de $q_2$ para $q_2$
  \item Lê {\tt 0}, segue a transição de $q_2$ para $q_3$
  \item Lê {\tt 1}, segue a transição de $q_3$ para $q_2$
  \item {\it Aceita}, pois $M_1$ está no estado de aceitação $q_2$ ao final da
        entrada.
\end{enumerate}
Simulando para as cadeias {\tt 1}, {\tt 01}, {\tt 11} e {\tt 0101010101} $M_1$
finalizará com {\it aceita}. De fato, $M_1$ aceita qualquer cadeia que termine
com o símbolo {\tt 1} deste que ela vai para o estado de aceitação sempre que
lê tal símbolo. Ademais, ela aceita as cadeias {\tt 100}, {\tt 110000} e {\tt
0101000000} e qualquer cadeia que termine com um número par de zeros após o
último {\tt 1}. Finalmente, ela rejeita cadeias como {\tt 0}, {\tt 10} e {\tt
101000}.

Quais são todas as cadeias que $M_1$ aceita? De outro modo, qual linguagem
$M_1$ aceita?

Um {\bf autômato finito} é uma $5$-upla $(Q,\Sigma,\delta,q_0,F)$, onde:
\begin{enumerate}
  \item $Q$ é um conjunto finito conhecido como {\bf estados}
  \item $\Sigma$ é um alfabeto
  \item $\delta$ é uma função $\delta:Q\times\Sigma\rightarrow Q$, chamada {\bf 
        função de transição}
  \item $q_0\in Q$ é o {\bf estado inicial}
  \item $F\subseteq Q$ é o {\bf conjunto de estados de aceitação} ou {\bf
        estados finais}
\end{enumerate}

Para a máquina $M_1$ acima temos
\begin{enumerate}
  \item $Q=\{q_1,q_2,q_3\}$
  \item $\Sigma=\{\mathtt{0,1}\}$
  \item $\delta$ é a função descrita por:\\
        \begin{tabular}{l|ll}
        & {\tt 0} & {\tt 1}\\
        \hline
        $q_1$ & $q_1$ & $q_2$ \\
        $q_2$ & $q_3$ & $q_2$ \\
        $q_3$ & $q_2$ & $q_2$ \\
        \end{tabular}
  \item $q_0$ é o estado inicial
  \item $F=\{q_2\}$
\end{enumerate}
Se $A$ é o conjunto de todas as cadeias que a máquina $M$ aceita, dizemos que
$A$ é a {\bf linguagem da máquina} $M$ e escrevemos $L(M)=A$. Dizemos ainda que
$M$ {\bf aceita} ou {\bf reconhece} $A$.

Para a máquina $M_1$ temos
\begin{eqnarray*}
A & = &\{w\in\{0,1\}^*;w\textrm{ contém pelo menos um }\mathtt{1}\\
  &   &\textrm{ e um número par de }\mathtt{0}s\textrm{ após o último 1}\}
\end{eqnarray*}

Considere o diagrama de estados do autômato finito $M_2$ abaixo
\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  \tikzstyle{every state}=[fill=black!10,draw=black,text=black]
    
  \node[initial,state] (q1) {$q_1$};
  \node[accepting,state] (q2) [right of=q1] {$q_2$};

  \path (q1) edge [loop above] node {0} (q1)
        (q2) edge [loop above] node {1} (q2)
        (q1.north east) [bend left=20]  edge  node {1} (q2.north west)
        (q2.south west) edge [below right] node {0} (q1.south east);
\end{tikzpicture}
\end{center}
Descreva $M_2$ formalmente. Qual a linguagem aceita por $M_2$?

Considere o diagrama de estados do autômato finito $M_3$ abaixo
\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  \tikzstyle{every state}=[fill=black!10,draw=black,text=black]
    
  \node[initial,state,accepting] (q1) {$q_1$};
  \node[state] (q2) [right of=q1] {$q_2$};

  \path (q1) edge [loop above] node {0} (q1)
        (q2) edge [loop above] node {1} (q2)
        (q1.north east) [bend left=20]  edge  node {1} (q2.north west)
        (q2.south west) edge [below right] node {0} (q1.south east);
\end{tikzpicture}
\end{center}
Descreva $M_3$ formalmente.  Qual a linguagem aceita por $M_3$?

Considere o diagrama de estados do autômato finito $M_4$ abaixo
\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  \tikzstyle{every state}=[fill=black!10,draw=black,text=black]
    
  \node[initial,state] (s) {$s$};
  \node[state,accepting] (q1) [below left  of=s] {$q_1$};
  \node[state] (q2) [below of=q1] {$q_2$};
  \node[state,accepting] (r1) [below right  of=s] {$r_1$};
  \node[state] (r2) [below of=r1] {$r_2$};

  \path (s) edge [above] node {a} (q1)
        (s) edge [above] node {b} (r1)
        (q1) edge [loop left] node {a} (q1)
        (q2) edge [loop left] node {b} (q2)
        (r1) [loop right] edge  node {b} (r1)
        (r2) [loop right] edge  node {a} (r2)
        (q1.south west) [bend right=20] edge  [left]  node {b} (q2.north west)
        (q2.north east) [bend right=20]  edge  [right] node {a} (q1.south east)
        (r1.south west) [bend right=20] edge  [left]  node {a} (r2.north west)
        (r2.north east) [bend right=20]  edge  [right] node {b} (r1.south east);
\end{tikzpicture}
\end{center}
Descreva $M_3$ formalmente.  Qual a linguagem aceita por $M_4$?

Considere o diagrama de estados do autômato finito $M_5$ abaixo
\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  \tikzstyle{every state}=[fill=black!10,draw=black,text=black]
    
  \node[state] (q1) {$q_1$};
 \node[initial,state,accepting] (q0) [below left of=q1,node
distance=6cm]{$q_0$};
  \node[state] (q2) [below right of=q1, node distance=6cm] {$q_2$};

  \path (q0) [loop above] edge [left] node {0,$<\mathtt{RST}>$} (q0)
        (q1) [loop above] edge [above] node {0} (q1)
        (q2) [loop right] edge [above] node {0} (q2)
        (q0) [bend left=15] edge [left] node {1} (q1)
        (q1) [bend left=15] edge [right] node {2,$<\mathtt{RST}>$} (q0)
        (q1) [bend left=15] edge [above] node {1} (q2)
        (q2) [bend left=15] edge [below] node {2} (q1)
        (q2) [bend left=15] edge [below] node {1,$<\mathtt{RST}>$} (q0)
        (q0) [bend left=15] edge [above] node {2} (q2);
\end{tikzpicture}
\end{center}
Descreva $M_5$ formalmente.  Qual a linguagem aceita por $M_5$?

\subsection{Definição formal de computação}
Seja $M$ um autômato finito $M=(Q,\Sigma,\delta,q_0,F)$ e suponha que 
$w=w_1w_2\cdots w_n$ seja uma cadeia onde cada $w_i$ é um símbolo de $\Sigma$.
Então $M$ aceita $w$ se existe uma sequência de estados $r_0,r_1,\ldots,r_n$ em
$Q$ tais que:
\begin{enumerate}
  \item $r_0=q_0$
  \item $\delta(r_i,w_{i+1})=r_{i+1}$ para $i=0,1,2,\ldots,n-1$
  \item $r_n\in F$
\end{enumerate}

Dizemos que $M$ {\bf reconhece a linguagem} $A$ se $A=\{w;M\textrm{ aceita }w\}$
Uma linguagem é chamada de {\bf linguagem regular} se algum autômato finito a
reconhece.

Por exemplo, a cadeia $w$ abaixo é aceita por $M_5$
$$\mathtt{10<RST>22<RST>012}$$

\begin{enumerate}
  \item Represente graficamente uma máquina que recebe uma cadeia sobre
        $\Sigma=\{\mathtt{0,1}\}$ e decide se a string possui um número par ou
        ímpar de $\mathtt{1}$s.
  \item Projete um autômato finito que aceita todas as $w$ cadeias sobre
        $\Sigma=\{\mathtt{0,1}\}$ tais que $\mathtt{001}$ é subcadeia de $w$.
\end{enumerate}

\subsection{Automatos Finitos Não-Determinísticos}
Considere a máquina $M_6$ 
\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  \tikzstyle{every state}=[fill=black!10,draw=black,text=black]
    
  \node[initial,state] (q1) {$q_1$};
  \node[state] (q2) [right of=q1] {$q_2$};
  \node[state,accepting] (q3) [right of=q2] {$q_3$};

  \path (q1) edge [loop above] node {0,1} (q1)
        (q1) edge [above] node {0} (q2)
        (q2) edge [above] node {0} (q3);
\end{tikzpicture}
\end{center}
e a máquina $M_7$ abaixo
\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  \tikzstyle{every state}=[fill=black!10,draw=black,text=black]
    
  \node[initial,state] (q1) {$q_1$};
  \node[state] (q2) [right of=q1] {$q_2$};
  \node[state] (q3) [right of=q2] {$q_3$};
  \node[state,accepting] (q4) [right of=q3] {$q_4$};

  \path (q1) edge [loop above] node {0,1} (q1)
        (q1) edge [above] node {1} (q2)
        (q2) edge [above] node {1,$\varepsilon$} (q3)
        (q3) edge [above] node {1} (q4)
        (q4) edge [loop above] node {0,1} (q4);
\end{tikzpicture}
\end{center}

Estas duas máquinas diferem dos autômatos finitos determinísticos em, pelo
menos, dois aspectos.
\begin{enumerate}
  \item Possuem mais de uma aresta com mesmo rótulo
  \item Possuem arestas rotuladas com $\varepsilon$
\end{enumerate}
Como tais máquinas se comportam (computam)? Se uma máquina, como as anteriores,
encontra-se em um estado e recebe um símbolo para o qual existem múltiplas
arestas (transições), então a máquina gera múltiplas ``cópias''\ de si mesma
cada uma indo para o próximo estado. Do mesmo modo, se existe uma aresta
$\varepsilon$, a máquina divide-se (antes de ler o próximo símbolo) levando
cada uma das cópias para um dos estados apontados por tal aresta.

Você pode pensar em tais máquinas como criando múltiplos ``processos'' ou
``{\it threads}'' nesses casos.

Mostraremos mais adiante que tais máquinas possuem uma equivalente sem setas
$\varepsilon$ e consistindo apenas de transições simples.

Formalmente, podemos descrever tais máquinas como segue.
$M=(Q,\Sigma_\varepsilon,\delta,q_0,F)$ onde
\begin{enumerate}
  \item $Q$ é um conjunto finito de estados
  \item $\Sigma_\varepsilon=\Sigma\cup\{\varepsilon\}$ e $\Sigma$ é um alfabeto.
  \item $\delta:Q\times\Sigma_\varepsilon\rightarrow\mathcal{P}(Q)$ é a função
        de transição.
  \item $q_0\in Q$ é o estado inicial.
  \item $F\subseteq Q$ é o conjunto de estados de aceitação.
\end{enumerate}
E chamamos $M$ de um {\bf autômato finito não-determinístico}
Para a máquina $M_6$ temos

\begin{enumerate}
  \item $Q=\{q_1,q_2,q_3,q_4\}$
  \item $\sigma=\{0,1\}$
  \item $\delta$ é a função dada como
      \begin{center}
        \begin{tabular}{l|ccc}
        & {\tt 0} & {\tt 1} & $\varepsilon$\\
        \hline
        $q_1$ & $\{q_1\}$   & $\{q_1,q_2\}$ & $\emptyset$\\
        $q_2$ & $\{q_3\}$   & $\emptyset$   & $\{q_3\}$\\
        $q_3$ & $\emptyset$ & $\{q_4\}$     & $\emptyset$\\
        $q_4$ & $\{q_4\}$   & $\{q_4\}$     & $\emptyset$\\
        \end{tabular}  
      \end{center}
  \item $q_1$ é o estado inicial
  \item $F=\{q_4\}$
\end{enumerate}

Seja $M$ um autômato finito não-determinístico $M=(Q,\Sigma,\delta,q_0,F)$ e
suponha que 
$w=w_1w_2\cdots w_n$ seja uma cadeia onde cada $w_i$ é um símbolo de
$\Sigma_\varepsilon$. Então $M$ aceita $w$ se existe uma sequência de estados
$r_0,r_1,\ldots,r_n$ em $Q$ tais que:
\begin{enumerate}
  \item $r_0=q_0$
  \item $r_{i+1}\in\delta(r_i,w_{i+1})$ para $i=0,1,2,\ldots,n-1$
  \item $r_n\in F$
\end{enumerate}
A condição 1 diz que começamos no estado inicial e a 3 diz que finalizamos em
um dos estados finais.
Note que $\delta(r_i,w_{i+1})$ é um conjunto, o conjunto de todos os estados
permissíveis.

\begin{teo}
Todo autômato finito não-determinístico possui um autômato finito determinístico
equivalente.
\end{teo}
\noindent{\bf Idéia da Demonstração:}\\
Primeiramente, você deve notar que saindo de um estado, chegamos a um conjunto
de estados. Se $M$ possui $n$ estados, então temos $2^n$ possiveis locais para
irmos.

Ainda precisamos descobrir a função de transição, o estado inicial e o conjunto
dos estados de aceitação.

\noindent{\bf Demonstração:}\\
Dado um autômato finito não-determinístico (AFN)
$M=(Q,\Sigma_\varepsilon,\delta,q_0,F)$, vamos gerar um autômato finito
determinístico (AFD) $N=(Q',\Sigma_{\varepsilon}',\delta',q_0',F')$ que realiza
a mesma computação que $M$.
\begin{enumerate}
  \item $Q=\mathcal{P}(Q)$, isto é, todo estado de $N$ é um conjunto de estados
        de $M$
  \item $F'=\{R\in Q';R\textrm{ contém algum estado de aceitação de }M\}$
\end{enumerate}
Agora precisamos definir a função de transição e o estado inicial.

Para tanto, dado $R\in N$, defina $E(R)$ como o conjunto dos estados que podem
ser alcançados, partindo-se de $R$, usando-se apenas setas $\varepsilon$
$$E(R)=\{q\in Q;q\textrm{ pode ser atingido a partir de }R\textrm{
usando-se }0\textrm{ ou mais setas }\varepsilon\}$$
Desse modo, temos agora
\begin{enumerate}\setcounter{enumi}{2}
  \item $q_0'=E(\{q_0\})$ é o conjunto que inclui o próprio $q_0$ bem como os
        estados que podem ser atingidos, a partir de $q_0$, utilizando-se setas
        $\varepsilon$ (zero ou mais).
  \item Finalmente, $\delta'(R,a)=\{q\in Q;q\in E(\delta(r,a))\textrm{ para
algum }r\in R\}$
\end{enumerate}


\end{document}
