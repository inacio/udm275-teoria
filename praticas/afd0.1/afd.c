#include <stdio.h>
#include <stdlib.h>
#define BUF 2048

int main( int argc, char **argv)
{
  int i, j, nEstados, nSimb, *tab, inicial, atual;
  FILE *inF;
  char *simb, c, string[BUF];

  argv++;argc--;
  if ( argc < 1 )
  {
    printf("Numero de argumentos insuficientes\n");
    printf("Sintaxe:\n %s <input_file>\n", argv[-1] );
    exit(1);
  }

  if ( !(inF = fopen( argv[0], "r" ) ) )
  {
    printf("Nao foi possivel abrir o arquivo %s\n", argv[0] );
    exit(2);
  }
  
  fscanf( inF, "%d%d", &nEstados, &nSimb ); 

  if ( ! (nEstados && nSimb) )
  {
    printf("Nao foi possivel obter numero de estados ou simbolos\n");
    exit(3);
  }
  simb = malloc( (nSimb+1) * sizeof(char) );
  tab  = malloc( nEstados*nSimb*sizeof(int) );

  for ( i=0; i<nSimb; i++ )
  {
    do{
      fscanf(inF, "%c", &c );
    }while ( c == '\n' || c==' ');
    simb[i]=c;
  }
  simb[i]='\0';
  printf("simbolos: %s\n",simb);

  for ( i=0; i<nEstados; i++ )
    for ( j=0; j<nSimb; j++ )
      fscanf(inF, "%d", tab+(nSimb*i+j) );

  for ( i=0; i<nEstados; i++ )
  {
    for ( j=0; j<nSimb; j++ )
      printf("%4d ", tab[nSimb*i+j] );
    printf("\n");
  }

  printf("Informe o estado inicial: ");
  scanf("%d", &inicial);
  atual=inicial;
 
  printf("Digite a cadeia: ");
  scanf("%s",string );

  for (i=0; i<strlen(string);i++)
  {
    c=string[i];
    printf("o caractere recebido foi %c\n",c);
    j=0;
    while ( simb[j] != c )
      j++;
    printf("Estado atual: %d ",atual);
    atual=tab[nSimb*atual+j];
    printf("indo para o estado: %d\n\n",atual);
  }

  return 0;
}
